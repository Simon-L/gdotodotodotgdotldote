# G.O.O.G.L.E

Analyse du trafic réseau que génère un navigateur web.

G.O.O.G.L.E écoute le trafic sur le port 443 (https) et à destination d'Internet, et retient chaque nouvelle adresse IP contactée. Pour chacune de ces adresses, le programme cherche dans une base de données le nom de l'organisation propriétaire de l'adresse IP, aussi appelée [Autonomous Sytem](https://en.wikipedia.org/wiki/Autonomous_system_%28Internet%29), qui est responsable de son attribution à une machine physique.  
Bien qu'une adresse IP publique telle que celle attribuée par un FAI à une connexion Internet de particulier ne l'identifie pas clairement, durablement et encore moins géographiquement (seul le FAI peut connaître cette information), il est possible de retrouver l'AS, le plus souvent une entreprise, propriétaire du bloc d'adresses auquel l'adresse appartient. Des informations publiques sont disponibles concernant ces blocs d'adresses, qui comprennent entre quelques centaines et plusieurs millions d'adresses, et les font correspondre à l'entreprise qui les possèdent. Ce programme fait usage de ces informations, formatées et mises à disposition par la société Maxmind.

#### Questions soulevées

Ce programme et les expériences futures entendent soulever des interrogations sur les enjeux entre autres politiques du fonctionnement d'Internet. Il met en lumière la domination du marché des serveurs et de l'hébergement par quelques entreprises autant qu'il révèle des noms bien méconnus du grand public qui jouent pourtant un rôle structurel critique. Bien que ces chiffres doivent être contextualisés, nuancés et expliqués, l'omniprésence des géants du secteur se fait immédiatement sentir dans les premiers résultats.

## Installation et utilisation

Extraire le contenu du fichier ASN.db.tar.gz dans le même dossier que le fichier GOOGLE.   
L'unique paramètre est l'interface internet sur laquelle écouter.

Sur Linux:
```
./GOOGLE -i eth0
```
Couper l'exécution avec `CTRL+C`, un fichier CSV est créé dans le dossier courant. Il est prêt à être utilisé par exemple dans Libre Office Calc, sélectionnez les deux colonnes de données à la fin du fichier, "ASN" et "Unique IPs" puis insérer un diagramme.

Exemple:
![diagramme exemple](https://pix.toile-libre.org/upload/original/1556053206.png)

## Développement

Le code source est écrit en Go et utilise les bibliothèques BoltDB, créée par Ben Johnson, et gopacket conçue par Google.

Le fonctionnement est largement simplifié pour cette première version de test. La base de donnée est rudimentaire, tout comme la recherche dans celle-ci, qui parcourt la totalité des paires clé-valeurs.

### Mentions légales

Ce programme est distribué selon les termes de la licence GPL v3.

Mention légale exigée par MaxMind, la société mettant à disposition la base de donnée :
This product includes GeoLite2 data created by MaxMind, available from
<a href="https://www.maxmind.com">https://www.maxmind.com</a>.

Selon ces termes, le fichier ASN.db (modifié depuis le fichier CSV de MaxMind, voir util/csv2bolt.go) est proposé sous la licence [CC by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0/)
