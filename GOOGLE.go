// GOOGLE: Google is an Omnipresent and Oppressive Giant, a Lifeless/Lawless Evil
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

var knownIPs map[string][]string
var stats map[string][]string

func main() {

	// Signal handling for interrupt
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigint
		// Catch SIGINT in order to save: see save.go
		save()
		log.Println("Exiting")
		os.Exit(0)
	}()

	knownIPs = make(map[string][]string)
	stats = make(map[string][]string)

	// Flag to select interface or help
	ifPtr := flag.String("i", "", "interface to use, such as eth0")
	flag.Parse()
	if *ifPtr == "" {
		fmt.Println("Usage:")
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Db functions wrapper: see db.go
	db := openDB()
	defer db.Close()

	log.Printf("Listening for trafic on %s, TCP on port 443 only...", *ifPtr)
	// https://godoc.org/github.com/google/gopacket/pcap#hdr-Reading_Live_Packets
	if handle, err := pcap.OpenLive(*ifPtr, 1600, true, pcap.BlockForever); err != nil {
		panic(err)
	} else if err := handle.SetBPFFilter("tcp and port 443"); err != nil { // optional
		panic(err)
	} else {
		packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
		for packet := range packetSource.Packets() {
			// https://godoc.org/github.com/google/gopacket#hdr-Basic_Usage
			ipLayer := packet.Layer(layers.LayerTypeIPv4)
			if ipLayer != nil {
				ip, _ := ipLayer.(*layers.IPv4)
				// if IP not in reserved private range...
				if !isPrivateAddress(ip.DstIP) {
					// lookup IP in known ASNs range, see: db.go
					go lookupASN(db, ip.DstIP)
				}
			}
		}
	}
}

// https://en.wikipedia.org/wiki/Private_network#Private_IPv4_addresses
func isPrivateAddress(ip net.IP) bool {
	_, privA, _ := net.ParseCIDR("10.0.0.0/8")
	_, privB, _ := net.ParseCIDR("172.16.0.0/12")
	_, privC, _ := net.ParseCIDR("192.168.0.0/16")
	return privA.Contains(ip) ||
		privB.Contains(ip) ||
		privC.Contains(ip)
}
