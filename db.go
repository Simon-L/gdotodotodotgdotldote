package main

import (
	"errors"
	"log"
	"net"

	"github.com/boltdb/bolt"
)

func openDB() *bolt.DB {
	// For better and faster access, db is opened read-only
	db, err := bolt.Open("ASN.db", 0600, &bolt.Options{ReadOnly: true})
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func lookupASN(db *bolt.DB, ip net.IP) {
	// Do not lookup if this IP is already known
	if v, ok := knownIPs[ip.String()]; ok == true {
		log.Printf("Connection to %s | ASN found: network %s owned by %s. (known)\n", ip, v[0], v[1])
	} else {
		db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("asn"))
			// XXX: Not very efficient way of looking up! ~429000 entries
			if err := b.ForEach(func(k, v []byte) error {
				_, netw, _ := net.ParseCIDR(string(k))
				// Contains returns true if IP is inside this network entry from the db
				if netw.Contains(ip) {
					log.Printf("Connection to %s | ASN found: network %s owned by %s.\n", ip, k, v)
					knownIPs[ip.String()] = []string{string(k), string(v)}
					// Returning an error stops the ForEach
					return errors.New("")
				}
				return nil
			}); err != nil {
				return err
			}
			return nil
		})
	}
}
