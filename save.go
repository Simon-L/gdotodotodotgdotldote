package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"time"
)

func save() {
	for k, v := range knownIPs {
		stats[v[1]] = append(stats[v[1]], k)
	}

	var records [][]string
	records = append(records, []string{"IP", "Network", "ASN"})
	for k, v := range knownIPs {
		records = append(records, []string{k, v[0], v[1]})
	}
	records = append(records, []string{""}) // Blank line

	records = append(records, []string{"ASN", "Unique IPs", "IPs..."})
	for k, v := range stats {
		records = append(records, append([]string{k, strconv.Itoa(len(v))}, v...))
	}

	t := time.Now()
	filename := "GOOGLE-" + t.Format("20060102150405") + ".csv"

	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := csv.NewWriter(f)
	w.WriteAll(records) // calls Flush internally
	if err := w.Error(); err != nil {
		log.Fatalln("error writing csv:", err)
	}

	log.Printf("Saved stats to %s", filename)
}
