// Lookup a host's IP in the ASN db
// Usage: ./hostlookup x.org
package main

import (
	"log"
  "errors"
  "os"
  "net"

	"github.com/boltdb/bolt"
)

func main() {

  ips, err := net.LookupHost(os.Args[1])
  if err != nil {
    panic(err)
  }
  log.Printf("Looking up ASN for host: %s with IP: %s", os.Args[1], ips[0])

	db, err := bolt.Open("ASN.db", 0600, &bolt.Options{ReadOnly: true})
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

  db.View(func(tx *bolt.Tx) error {
  	b := tx.Bucket([]byte("asn"))
    if err := b.ForEach(func(k, v []byte) error {
        _, netw, _ := net.ParseCIDR(string(k))
        if netw.Contains(net.ParseIP(ips[0])) {
          log.Printf("ASN found for %s: %s owned by %s.\n", os.Args[1], k, v)
          return errors.New("")
        }
        return nil
    }); err != nil {
        return err
    }
  	return nil
  })
}
