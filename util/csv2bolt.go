// Run alongside a copy of the ASN Blocks IPv4 CSV file (https://dev.maxmind.com/geoip/geoip2/geolite2/)
// WARNING! this program is verbose and takes a long time, it's a simple one by one dump of CSV into a Bolt DB bucket

package main

import (
	"encoding/csv"
	"io"
	"log"
  "os"

  "github.com/boltdb/bolt"
)

func main() {
  f, err := os.Open("GeoLite2-ASN-Blocks-IPv4.csv")
  if err != nil {
    panic(err)
  }
	r := csv.NewReader(f)

  db, err := bolt.Open("ASN.db", 0600, nil)
  if err != nil {
    log.Fatal(err)
  }
  defer db.Close()

  db.Update(func(tx *bolt.Tx) error {
    b, err := tx.CreateBucket([]byte("asn"))
    for {
      record, err := r.Read()
      if err == io.EOF {
        break
      }
      if err != nil {
        log.Fatal(err)
      }

      log.Println(record)
      err = b.Put([]byte(record[0]), []byte(record[2]))
      if err != nil {
        panic(err)
      }
    }
  	return err
  })

}
